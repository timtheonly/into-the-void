﻿using UnityEngine;
using System.Collections.Generic;


namespace intoTheVoid
{
	public class Waypoints
	{
		private List<Vector3> _points = new List<Vector3>();
		public List<Vector3> points {
			get{ return this._points;}
			set{ this._points = value;}
		}

		private int _current = 0;
		public int current {
			get { return this._current; }
			set { this._current = value; }
		}

		public Vector3 getCurrentWaypoint()
		{
			return points[current];
		}

		public void advance()
		{
			current = (current + 1) % points.Count;
		}
			
	}
}
