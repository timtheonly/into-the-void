﻿using UnityEngine;
using System.Collections;

namespace intoTheVoid
{
	public class camfollow : MonoBehaviour {

		private GameObject _target;
		private float smooth = 5.0f;

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			Vector3 wantedPosition = target.transform.TransformPoint (0, 5.0f, 10.0f);
			transform.position = Vector3.Lerp (transform.position, wantedPosition, Time.deltaTime * smooth);
			transform.LookAt (target.transform, target.transform.up);
		}

		public GameObject target{
			set {this._target = value; }
			get {return this._target; }
		}
	}
}