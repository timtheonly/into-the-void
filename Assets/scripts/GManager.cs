﻿using UnityEngine;
using System.Collections.Generic;

namespace intoTheVoid
{
	public class GManager : MonoBehaviour {

		private GameObject leader;
		private GameObject follower;
		private Camera mainCamera;

		private List<Vector3> points = new List<Vector3>();

		// Use this for initialization
		void Start () {

			points.Add(new Vector3(150.0f, 5.0f, 100.0f));
			points.Add(new Vector3(300.0f, 5.0f, 150.0f));
			points.Add(new Vector3(450.0f, 5.0f, 250.0f));
			points.Add(new Vector3(150.0f, 5.0f, 300.0f));

			leader = (GameObject)Instantiate(Resources.Load ("boid"), Vector3.zero, Quaternion.identity);
			leader.GetComponent<SteeringBehaviours>().enableBehaviour (Behaviours.path);
			leader.GetComponent<SteeringBehaviours>().path.points = points;

			follower = (GameObject)Instantiate(Resources.Load("boid"), new Vector3(-100.0f, 5.0f, 100.0f), Quaternion.identity);
			follower.GetComponent<SteeringBehaviours>().enableBehaviour(Behaviours.persuit);
			follower.GetComponent<SteeringBehaviours>().targetObject = leader;

			mainCamera = Camera.main;
			mainCamera.GetComponent<camfollow>().target = leader;
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}