﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace intoTheVoid
{
	public class SteeringBehaviours : MonoBehaviour
	{
		private Behaviours behavioursMask;
		private Vector3 force, velocity, acceleration, _targetPos, _offset;
		private Waypoints _path = new Waypoints();
		private float _mass, _maxSpeed;
		private GameObject _targetObject;

		void Start(){
			mass = 1.0f;
			maxSpeed = 50.0f;
			force = Vector3.zero;
			velocity = Vector3.zero;
			acceleration = Vector3.zero;
		}

		void Update()
		{
			if(isBehaviourEnabled(Behaviours.arrive)) {
				force = arrive();
			}
			else if(isBehaviourEnabled(Behaviours.seek)) {
				force = seek();
			}
			else if(isBehaviourEnabled(Behaviours.flee)) {
				force = flee();
			}
			else if(isBehaviourEnabled(Behaviours.path)) {
				force = followPath();
			}
			else if(isBehaviourEnabled(Behaviours.persuit)) {
				force = persue();
			}
		
			acceleration = force/mass;

			velocity += acceleration * Time.deltaTime;
			transform.position += velocity * Time.deltaTime;

			float speed = velocity.magnitude;
			if (speed > Mathf.Epsilon) {
				transform.forward = velocity;
				transform.forward.Normalize();
			}
		}
			
		public Waypoints path {
			get{ return _path; }
			set{ this._path = value; }
		}

		public float mass{
			get{ return _mass;}
			set{ this._mass = value;}
		}

		public float maxSpeed{
			get{ return _maxSpeed;}
			set { this._maxSpeed = value;}
		}

		public Vector3 targetPos{
			get{ return _targetPos;}
			set { this._targetPos = value;}
		}

		public GameObject targetObject{
			get{ return _targetObject;}
			set{ this._targetObject = value;}
		}

		public bool isBehaviourEnabled(Behaviours behaviour)
		{
			return(behaviour & behavioursMask) != 0;
		}

		public void enableBehaviour(Behaviours behaviour){
			behavioursMask |= behaviour;
		}

		public void disableBehaviour(Behaviours behaviour){
			behavioursMask &= behaviour;
		}

		public void disableAllBehavoirs()
		{
			behavioursMask = Behaviours.none;
		}

		private Vector3 arrive(){
			Vector3 toTarget = targetPos - transform.position;

			float distance = toTarget.magnitude;
			if (distance < Mathf.Epsilon) {
				return Vector3.zero;
			}

			float slowingRadius = 8.0f;
			float decelerationTweaker = 12.3f;

			float ramped = maxSpeed * (distance / (slowingRadius * decelerationTweaker));

			float clamped = Math.Min (ramped, maxSpeed);

			Vector3 desired = clamped * (toTarget / distance);

			return desired - velocity;
		}

		private Vector3 seek(){
			Vector3 desired = Vector3.Normalize(targetPos - transform.position) * maxSpeed;
			return (desired - velocity);
		}

		private Vector3 flee(){
			Vector3 desired = Vector3.Normalize(transform.position - targetPos) * maxSpeed;
			return (desired - velocity);
		}

		private Vector3 persue()
		{
			Vector3 toTarget = targetObject.transform.position - transform.position;
			float ahead = toTarget.magnitude / maxSpeed;
			targetPos = targetObject.transform.position + (targetObject.GetComponent<SteeringBehaviours>().velocity * ahead);
			return seek();
		}

		private Vector3 followPath()
		{
			float dist = (transform.position - path.getCurrentWaypoint()).magnitude;
			if(dist < 0.5f) {
				path.advance();
			}
			targetPos = path.getCurrentWaypoint();
			return seek();
		}
	}
}

