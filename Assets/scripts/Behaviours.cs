﻿using System;

namespace intoTheVoid
{
	public enum Behaviours
	{
		none = 0,
		arrive = 1,
		seek = 2,
		flee = 4,
		avoid = 8,
		persuit = 16,
		evade = 32,
		flock = 64,
		path = 128,
	}
}

